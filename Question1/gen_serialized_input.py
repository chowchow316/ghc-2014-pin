import pickle

class StopwatchRecord:
	"""A class to store the timing information of stop watch records"""
	def __init__(self, key, start_time, end_time):
		self.key = key
		self.start_time = start_time
		self.end_time = end_time
	def __repr__(self):
		return '{}: {} {}'.format(self.__class__.__name__, self.key, self.start_time, self.end_time)

with open('input.pkl', 'wb') as output:
	record1 = StopwatchRecord('render_module', 101.0, 105.0)
	pickle.dump(record1, output, pickle.HIGHEST_PROTOCOL)
	record2 = StopwatchRecord('get_data', 101.1, 101.2)
	pickle.dump(record2, output, pickle.HIGHEST_PROTOCOL)
	record3 = StopwatchRecord('get_data', 101.5, 103.0)
	pickle.dump(record3, output, pickle.HIGHEST_PROTOCOL)
	record4 = StopwatchRecord('cache_get_data', 101.6, 101.7)
	pickle.dump(record4, output, pickle.HIGHEST_PROTOCOL)
	record5 = StopwatchRecord('db_get_data', 101.8, 102.9)
	pickle.dump(record5, output, pickle.HIGHEST_PROTOCOL)
	record6 = StopwatchRecord('render_module', 103.1, 104.9)
	pickle.dump(record6, output, pickle.HIGHEST_PROTOCOL)
	record7 = StopwatchRecord('render_module', 105.1, 106.0)
	pickle.dump(record7, output, pickle.HIGHEST_PROTOCOL)