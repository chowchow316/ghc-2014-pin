# Read file from sample input and pretty print them
# By Qian Sun.

import pickle

class StopwatchRecord:
	"""A class to store the timing information of stop watch records"""
	def __init__(self, key, start_time, end_time):
		self.key = key
		self.start_time = start_time
		self.end_time = end_time
	def __repr__(self):
		return '{}: {} {}'.format(self.__class__.__name__, self.key,
                                  self.start_time, self.end_time)

# create a list of objects to store the input
recordList = []
# assume the input is serialized using pickle
input = open('input.pkl', 'rb')
try:
	while True:
	# deserialize the objects in input file
		record = pickle.load(input)
		recordList.append(record)
except EOFError:
	print 'End of file reached'
input.close()

# sort the record according to the starting time
sorted(recordList, key = lambda x: x.start_time)

# pretty print the records 
stack = []
for record in recordList:
	while(stack and record.end_time > stack[-1]): stack.pop()
	n = len(stack) 
	indent = "\t"*n
	print ("%.1f" % record.start_time) + indent + \
			" [" + ("%.1f" % (record.end_time - record.start_time)) + "] " + record.key
	stack.append(record.end_time)




