import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class SecondConnection {
	public enum SocialNetwork {PINTEREST, FACEBOOK, TWITTER};

	public static void main(String[] args) {
		//Cross-source second degree connection

		HashMap<Integer, String> socialMap = new HashMap<Integer, String>();
		socialMap.put(0, "PINTEREST");
		socialMap.put(1, "FACEBOOK");
		socialMap.put(2, "TWITTER");

		List<Integer> cur = new ArrayList<Integer>();
		ArrayList<List<Integer>> res = new ArrayList<List<Integer>>();
		ArrayList<List<Integer>> connectUser1 = new ArrayList<List<Integer>>();
		ArrayList<List<Integer>> connectUser2 = new ArrayList<List<Integer>>();

		int userId1, userId2;

		System.out. println("Please input ID of use A: ");
		Scanner in = new Scanner(System.in);
		userId1 = in.nextInt();		
		System.out. println("Please input ID of use B: ");
		userId2 = in.nextInt();
		in.close(); 		

		for(SocialNetwork sol : SocialNetwork.values()){		
			cur = getConnections(userId1, sol);
			connectUser1.add(cur);
			cur = getConnections(userId2, sol);
			connectUser2.add(cur);
		}

		int i, j;
		for(i = 0; i < 3; i++) {
			for (j = 0; j < 3; j++) {
				if(i == j) continue;
				List<Integer> tmpA = connectUser1.get(i);
				List<Integer> tmpB = connectUser2.get(j);
				if(intersection(tmpA, tmpB)) {
					List<Integer> original = Arrays.asList(i, j);
					res.add(original);		
				}
			}
		}	
		for (List <Integer> ele: res) 
			System.out.println("[" + socialMap.get(ele.get(0)) + "," + socialMap.get(ele.get(1)) + "]");
	}

	public static boolean intersection(List<Integer> list1, List<Integer> list2) {
		for (Integer t : list1) {
			if(list2.contains(t)) return true;
		}
		return false;
	}

	public static List<Integer> getConnections(int userId, SocialNetwork socialNetwork) {
		// hard coded for sample test
		List<Integer> ans = new ArrayList<Integer>();
		if(userId == 1) {
			switch(socialNetwork){
			case PINTEREST: ans.add(3);break;
			case FACEBOOK: ans.add(4);break;
			case TWITTER: ans.add(4);break;
			default: break;
			}
		}

		if(userId == 2) {
			switch(socialNetwork){
			case PINTEREST: ans.add(4);break;
			case FACEBOOK: ans.add(3);break;
			default: break;
			}
		}
		return ans;

	}

}

