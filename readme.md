#### Question 1
This folder is for question 1 in the code challenge, and questionOne.py is the main funtion for solving the problem.

List of files:

 - `gen_serialized_input.py`: To generate the serizlized objects to file.
 - `input.pkl`: The input file generalized by `gen_serialized_input.py`.
 - `pretty_print.py`: Main solution to the challenge.


The input can be serialized and stored in files using pickle (input.pkl), and I also provided the code for writting the serialized objects to file (genSerializedInput.py). For large input data, the serialized storage can help save space.

#### Question 2
In this folder, there are only one java file for solving the challenge, in which I hard-coded the `getConnections()` function for testing.
When running the code, please type 1 and 2 as two user IDs to get the required sample output.
